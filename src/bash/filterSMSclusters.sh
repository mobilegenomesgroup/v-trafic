#!/bin/bash

clusters=$1
tumourbam=$2
outputdir=$3
projectid=$4
donorid=$5
tumourid=$6

prevfilename=$(basename "$clusters")
prevfilename="${prevfilename%.*}"
filename=$(sed 's/prev//g' <<< $prevfilename)


cat $clusters | while read line
do
	coord=$(echo $line | awk '{print $1":"$2"-"$3}')


	samtools view $tumourbam $coord > $outputdir/$projectid/$donorid/$tumourid/$filename.tempbam.txt

	mapqReads=$( awk '$5 <10 {print}' $outputdir/$projectid/$donorid/$tumourid/$filename.tempbam.txt | wc -l)

	smsReads=$(awk '($6 ~ /^[0-9]*S[0-9]*M[0-9]*S/){print}' $outputdir/$projectid/$donorid/$tumourid/$filename.tempbam.txt | wc -l)

	totalReads=$( cat $outputdir/$projectid/$donorid/$tumourid/$filename.tempbam.txt | wc -l)



	mapqPercentage=$(bc <<<"scale=2; $mapqReads / $totalReads")
	smsPercentage=$(bc <<<"scale=2; $smsReads / $totalReads")

	mapqLimit=0.3
	smsLimit=0.15
	if (( $(bc <<< "$mapqPercentage <= $mapqLimit") )); then
		if (( $(bc <<< "$smsPercentage <= $smsLimit") )); then
			echo $line | sed 's/ /\t/g' >> $outputdir/$projectid/$donorid/$tumourid/$filename.woSMS.txt
		fi
	fi
done

rm $outputdir/$projectid/$donorid/$tumourid/$filename.tempbam.txt

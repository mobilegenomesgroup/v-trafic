#!/bin/bash

blast10=$1
int10=$2
blast100=$3
int100=$4
clusters=$5
final10=$6
final100=$7

grep -A1 hit $blast10 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int10
if [ -s $clusters ]; then awk -v file="$int10" 'BEGIN{FS="\t"}{if (FILENAME==file) {hash[$1_$2_$3]=$4} else {{if (hash[$1_$2_$3]!="") print $0"\t"hash[$1_$2_$3]}}}' $int10 $clusters > $final10; else touch $final10; fi

grep -A1 hit $blast100 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int100
if [ -s $clusters ]; then awk -v file="$int100" 'BEGIN{FS="\t"}{if (FILENAME==file) {hash[$1_$2_$3]=$4} else {{if (hash[$1_$2_$3]!="") print $0"\t"hash[$1_$2_$3]}}}' $int100 $clusters > $final100; else touch $final100; fi

#!/bin/bash

pathcontigs=$1
outputdir=$2
projectid=$3
donorid=$4
tumourid=$5

contigs=$(basename "$pathcontigs")

touch $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits10.txt
touch $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits100.txt

cat $outputdir/$projectid/$donorid/$tumourid/$contigs\_blast.txt | while read line
do
	if [[ $line == \#* ]]; then
		if [[ $line == *"hits found"* ]]; then
			hits=$(echo $line | awk '{print $2}')
			if [[ $hits -gt 10 ]]; then
				echo "$line" >> $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits10.txt
				impri10="yes"
			fi
			if [[ $hits -gt 99 ]]; then
				echo "$line" >> $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits100.txt
				impri100="yes"
			fi
		else
			impri10="no"
			impri100="no"
		fi
	else
		if [[ $impri10 == "yes" ]]; then
			echo "$line" >> $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits10.txt
		fi
		if [[ $impri100 == "yes" ]]; then
			echo "$line" >> $outputdir/$projectid/$donorid/$tumourid/$contigs.blastFiltered_hits100.txt
		fi
	fi
done

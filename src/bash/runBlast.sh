#!/bin/bash

if [ $# -eq 7 ]
then

	ncbipath=$1
	db=$2
	pathcontigs=$3
	outputdir=$4
	projectid=$5
	donorid=$6
	tumourid=$7

	contigs=$(basename "$pathcontigs")

	$ncbipath/blastn -db $db -query $outputdir/$projectid/$donorid/$tumourid/$contigs -outfmt "7 qseqid stitle pident length mismatch gapopen qstart qend sstart send evalue bitscore" -out $outputdir/$projectid/$donorid/$tumourid/$contigs\_blast.txt
else
	db=$1
	pathcontigs=$2
	outputdir=$3
	projectid=$4
	donorid=$5
	tumourid=$6

	contigs=$(basename "$pathcontigs")

	blastn -db $db -query $outputdir/$projectid/$donorid/$tumourid/$contigs -outfmt "7 qseqid stitle pident length mismatch gapopen qstart qend sstart send evalue bitscore" -out $outputdir/$projectid/$donorid/$tumourid/$contigs\_blast.txt
fi

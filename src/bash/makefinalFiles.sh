#!/bin/bash

blast10=$1
int10=$2
blast100=$3
int100=$4
clusters=$5
final10=$6
final100=$7
typeClusters=$8


if [ $typeClusters == "reciprocal" ]; then
	grep -A1 hit $blast10 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int10
	if [ ! -s $int10 ]; then echo "empty" > $int10; fi
	if [ -s $clusters ]; then
		cat $int10 | while read line
		do
			chr=$(echo "$line" | awk -F"\t" '{print $1}')
			pos1=$(echo "$line" | awk -F"\t" '{print $2}')
			pos2=$(echo "$line" | awk -F"\t" '{print $3}')
			org=$(echo "$line" | awk -F"\t" '{print $4}')
			awk -v chr="$chr" -v pos1="$pos1" -v pos2="$pos2" -v org="$org" '{if($1 == chr && $3 == pos1 && $7 == pos2){print $0"\t"org}}' $clusters >> $final10
		done
	fi
	if [ ! -s $final10 ]; then echo "NO INTEGRATIONS" > $final10; fi

	grep -A1 hit $blast100 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int100
	if [ ! -s $int100 ]; then echo "empty" > $int100; fi
	if [ -s $clusters ]; then
		cat $int100 | while read line
		do
			chr=$(echo "$line" | awk -F"\t" '{print $1}')
			pos1=$(echo "$line" | awk -F"\t" '{print $2}')
			pos2=$(echo "$line" | awk -F"\t" '{print $3}')
			org=$(echo "$line" | awk -F"\t" '{print $4}')
			awk -v chr="$chr" -v pos1="$pos1" -v pos2="$pos2" -v org="$org" '{if($1 == chr && $3 == pos1 && $7 == pos2){print $0"\t"org}}' $clusters >> $final100
		done
	fi
	if [ ! -s $final100 ]; then echo "NO INTEGRATIONS" > $final100; fi
fi

if [ $typeClusters == "IC" ]; then
	grep -A1 hit $blast10 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int10
	if [ ! -s $int10 ]; then echo "empty" > $int10; fi
	if [ -s $clusters ]; then
		cat $int10 | while read line
		do
			chr=$(echo "$line" | awk -F"\t" '{print $1}')
			pos1=$(echo "$line" | awk -F"\t" '{print $2}')
			pos2=$(echo "$line" | awk -F"\t" '{print $3}')
			org=$(echo "$line" | awk -F"\t" '{print $4}')
			awk -v chr="$chr" -v pos1="$pos1" -v pos2="$pos2" -v org="$org" '{if($1 == chr && $2 == pos1 && $3 == pos2){print $0"\t"org}}' $clusters >> $final10
		done
	fi
	if [ ! -s $final10 ]; then echo "NO INTEGRATIONS" > $final10; fi

	grep -A1 hit $blast100 | sed 's/\t/_/g' | awk 'BEGIN{FS="_";OFS="\t"}{print $7,$8,$9,$10}' > $int100
	if [ ! -s $int100 ]; then echo "empty" > $int100; fi
	if [ -s $clusters ]; then
		cat $int100 | while read line
		do
			chr=$(echo "$line" | awk -F"\t" '{print $1}')
			pos1=$(echo "$line" | awk -F"\t" '{print $2}')
			pos2=$(echo "$line" | awk -F"\t" '{print $3}')
			org=$(echo "$line" | awk -F"\t" '{print $4}')
			awk -v chr="$chr" -v pos1="$pos1" -v pos2="$pos2" -v org="$org" '{if($1 == chr && $2 == pos1 && $3 == pos2){print $0"\t"org}}' $clusters >> $final100
		done
	fi
	if [ ! -s $final100 ]; then echo "NO INTEGRATIONS" > $final100; fi
fi
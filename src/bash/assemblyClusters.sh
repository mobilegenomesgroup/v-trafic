#!/bin/bash

inputdir=$1
projectid=$2
donorid=$3
tumourid=$4
reciprocal=$5
allunmdifchrwoNST=$6
fastaName=$7
velvetpath=$8

if [ -s "$reciprocal" ]
then
	cat $reciprocal | while read line
	do
		chr=$(echo $line | awk '{print $1}')
                posCoord=$(echo $line | awk '{print $3}')
                negCoord=$(echo $line | awk '{print $7}')
		echo $line | awk '{print $5","$10}' | sed 's/,/\n/g' > $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReadsIDs.txt
		grep -A1 -Ff $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReadsIDs.txt $inputdir/$projectid/$donorid/$tumourid/$tumourid.all_readswoNS.txt | sed 's/--//g' | awk 'NF' | awk '{print ">"$1"\n"$4}' > $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReads.fa
		$velvetpath/velveth $inputdir/$projectid/$donorid/$tumourid/$chr\_$posCoord\_$negCoord 21 -fasta -short $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReads.fa 1> $inputdir/$projectid/$donorid/$tumourid/velveth.out
		$velvetpath/velvetg $inputdir/$projectid/$donorid/$tumourid/$chr\_$posCoord\_$negCoord -min_contig_lgth 150 -exp_cov auto -cov_cutoff auto 1> $inputdir/$projectid/$donorid/$tumourid/velvetg.out
		if [ -s $inputdir/$projectid/$donorid/$tumourid/$chr\_$posCoord\_$negCoord/contigs.fa ]
		then
			awk -v chr="$chr" -v posCoord="$posCoord" -v negCoord="$negCoord" '/^>/ {$0=$0 "_" chr"_"posCoord"_"negCoord}1' $inputdir/$projectid/$donorid/$tumourid/$chr\_$posCoord\_$negCoord/contigs.fa >> $inputdir/$projectid/$donorid/$tumourid/$fastaName
		fi
		rm -r $inputdir/$projectid/$donorid/$tumourid/$chr\_$posCoord\_$negCoord
	done
fi

rm $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReads.fa
rm $inputdir/$projectid/$donorid/$tumourid/$fastaName.clusterReadsIDs.txt
rm $inputdir/$projectid/$donorid/$tumourid/velveth.out
rm $inputdir/$projectid/$donorid/$tumourid/velvetg.out

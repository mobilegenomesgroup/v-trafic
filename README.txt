##############################################################################################
# v-TraFiC v0.23
# Look for viral integrations in tumour genomes
##############################################################################################

# Operating systems:
Unix and MacOS

# Following external tools must be installed:
ncbi-blast  https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download
velvet  https://www.ebi.ac.uk/~zerbino/velvet/
snakemake   https://snakemake.readthedocs.io/en/stable/

# v-TraFiC does not require installation, just download source files.

# Tested with:
ncbi-blast v2.6.0
velvet v1.2.10
snakemake v3.6

# Demo dataset:
Bam file with human reads (hg19) containing viral integrations (demo_hg19_v-TraFiC_chr22.bam)

# HOW TO RUN v-TraFiC

## 1. Set tool paths in VTRAFIC file:
itrafic_path=
ncbi_path=
velvetpath=

## 2. Create configuration file (example.yaml) containing the following information:

# Project name:
project: 
# Sample name:
sample_name: 
# Tumour name (same as sample name for non-paired runs)
tumour_name:
# Path to tumour bam:
tumour_bam:
# Normal name if normal sample is provided, otherwise normal_name: ''
normal_name:
# Path to normal bam if normal sample is provided, otherwise normal_bam: ''
normal_bam:
# Path to output directory:
output_dir:
# Path to viral database:
db:
# If mitochodrial chromosome is required MT: 'yes', otherwise MT: 'no'
MT:

## 3. Run with snakemake (all snakemake options can be used):

snakemake --configfile PATH/TO/example.yaml --snakefile PATH/TO/VTRAFIC --keep-target-files --nolock --notemp

## 4. The output files to inspect are output_path/INTEGRATIONS_out.

Integrations are distributed according to their orientation in 3 files - RajireciprocalIntegrations.txt, RajiICposIntegrations.txt and RajiICnegIntegrations.txt - being tab delimited files with the following structure:

chromosome_bkp1	coordinate1	coordinate2 number_of_supporting_reads_bkp1	name_of_supporting_reads_bkp1	chromosome_bkp2	coordinate1	coordinate2 number_of_supporting_reads_bkp2	name_of_supporting_reads_bkp2	viral_sequence_identifier.

## 5. Computational requirements

v-TraFiC takes about 12 CPU hours and 8Gb of RAM (we recommend 15Gb) to analyse a 30X whole sequence human genome on 1 CPU.
Test dataset should run in 20 minutes with same conditions as above.
